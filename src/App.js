import React, { useState } from 'react';

const TodoList = (props) => {
    const onDelete = (todoItem) => {
        props.handleDelete(todoItem);
    }

    return (
        <ul>
            <li>{props.todoItem.item} <button onClick={() => onDelete(props.todoItem)}>Delete</button></li>
        </ul>
    );
}

export const TodoApp = (props) =>  {
    const [todoList, setTodoList] = useState([]);
    const [text, setText] = useState('');

    const handleChange = e => {
        setText(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault();
        if(text === '' || todoList === [])
            return
        const item = {id: todoList.length+1, item: text};
        setTodoList(todoList.concat(item));
        setText('');
    }

    const handleDelete = (todoItem) => {
        const newList = todoList.filter(item => item !== todoItem)
        setTodoList(newList)
    }

    return(
        <div>
            <h3>TODO</h3>

            {todoList.map(todo => 
                <TodoList key={todo.id} todoItem={todo} handleDelete={handleDelete}/>
            )}

            <form onSubmit={handleSubmit}>
                <label htmlFor="new-todo">
                    What needs to be done?
                </label><br /><br />
                <input type="text" value={text} onChange= {handleChange}></input><br /><br />
                <button type="submit">
                    Add{todoList.count > 0 ? '#'+todoList.count : '' }
                </button>
            </form>
        </div>
    );
}
